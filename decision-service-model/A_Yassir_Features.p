PredicatesModel _HE7uoV7oEeqP-IMIOa1IZQ {
  Predicate _lr5mAF7qEeqXlZpgZ7Ab5w at 110,100 size 297,76 {
  	name "isValidIDFormat"
  }
  
  Predicate _tVMSoV7qEeqXlZpgZ7Ab5w at 69,260 size 240,76 {
  	name "isNumeric"
  }
  
  Predicate _EPOjEWEoEeqeQ6gvu3M62w at 550,57 size 240,76 {
  	name "isMale"
  }
  
  Predicate _jmTeMWEpEeqeQ6gvu3M62w at 550,150 size 240,76 {
  	name "isFemale"
  }
  
  Predicate _V7MU4WEwEeqeQ6gvu3M62w at 55,440 size 268,76 {
  	name "isValidAge"
  }
  
  Predicate _CRqCIWEzEeqeQ6gvu3M62w at 1090,40 size 240,76 {
  	name "isMarried"
  }
  
  Predicate _Cgbd8WEzEeqeQ6gvu3M62w at 1090,110 size 240,76 {
  	name "isWidower"
  }
  
  Predicate _CtIdMWEzEeqeQ6gvu3M62w at 1090,183 size 240,76 {
  	name "isBachelor"
  }
  
  Predicate _C5feMWEzEeqeQ6gvu3M62w at 1090,260 size 240,76 {
  	name "isSingle"
  }
  
  Predicate _fT_okWE2EeqeQ6gvu3M62w at 580,460 size 283,76 {
  	name "isPresentAtDeath"
  }
  
  Predicate _hlYcYWE2EeqeQ6gvu3M62w at 571,550 size 333,76 {
  	name "isNotPresentAtDeath"
  }
  
  Predicate _wc9pwWE4EeqeQ6gvu3M62w at 1080,460 size 263,76 {
  	name "isValidInformant"
  }
  
  Predicate _0crAkWE4EeqeQ6gvu3M62w at 1080,550 size 319,76 {
  	name "isQualifiedInformant"
  }
  
  Predicate _9eHL4WE4EeqeQ6gvu3M62w at 1067,640 size 383,76 {
  	name "isValidInformantAddress"
  }
  
  Predicate _uFkwAWIGEeqHEMcDVmTZNA at 20,640 size 477,76 {
  	name "isRegistrationDateOutOfRange"
  }
  
  Predicate _9OmlAXD2EeqJtZ15MBniJw at 1440,57 size 248,76 {
  	name "isBaker"
  }
  
  Predicate _mbv7cXD6EeqJtZ15MBniJw at 1440,140 size 248,76 {
  	name "isFarmer"
  }
  
  Predicate _qGZoQXD6EeqJtZ15MBniJw at 1440,215 size 248,76 {
  	name "isHousekeeper"
  }
  
  Predicate _tRmY8XD6EeqJtZ15MBniJw at 1440,290 size 248,76 {
  	name "isLady"
  }
  
  Predicate _wMKsoXD6EeqJtZ15MBniJw at 1440,365 size 248,76 {
  	name "isTeacher"
  }
  
  Predicate _01qAQXD6EeqJtZ15MBniJw at 1440,440 size 248,76 {
  	name "isLabourer"
  }
  
  Predicate _bluQcXEjEeqJtZ15MBniJw at 1840,57 size 264,76 {
  	name "isCardiacFailure"
  }
  
  Predicate _cKX1oXEjEeqJtZ15MBniJw at 1840,140 size 248,76 {
  	name "isAsthenia"
  }
  
  Predicate _c27ZoXEjEeqJtZ15MBniJw at 1848,225 size 248,76 {
  	name "isPhthisis"
  }
  
  Predicate _enVAQXEjEeqJtZ15MBniJw at 1848,310 size 248,76 {
  	name "isMeningitis"
  }
  
  Predicate _fSAU8XEjEeqJtZ15MBniJw at 1848,390 size 248,76 {
  	name "isDebility"
  }
  
  Predicate _gYGaUXEjEeqJtZ15MBniJw at 1848,480 size 248,76 {
  	name "isSenility"
  }
  
  Predicate _4WbasXEuEeqJtZ15MBniJw at 2230,80 size 359,76 {
  	name "isCauseOfDeath1Valid"
  }
  
  Predicate _4s6oEXEuEeqJtZ15MBniJw at 2240,183 size 359,76 {
  	name "isCauseOfDeath2Valid"
  }
  
  Predicate _GLYtcXExEeqJtZ15MBniJw at 2296,590 size 248,76 {
  	name "isCertified"
  }
  
  Predicate _yjSAQXFCEeqJtZ15MBniJw at 2180,800 size 427,76 {
  	name "isDateOfDeathOutOfRange"
  }
  
  Predicate _VVwr0XGzEeqJtZ15MBniJw at 1135,850 size 248,76 {
  	name "isLadyFemale"
  }
}
